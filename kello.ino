#include <TimeLib.h>
#include <LiquidCrystal.h>
#include <Servo.h>
Servo servo;

LiquidCrystal lcd(12, 11, 7, 8, 9, 10);

int buzzer = 13, set = 2, nxt = 3, left = 4, mid = 5, right = 6; // declare pins 
int hms[] = {0, 0, 0};
char aika[16];
int haly = 0, tch = 0;
int reset;

void setup() {
  servo.attach(A0);
  // set pin modes 
  pinMode(buzzer, OUTPUT);
  pinMode(set, INPUT_PULLUP);
  pinMode(nxt, INPUT_PULLUP);
  pinMode(left, INPUT_PULLUP);
  pinMode(mid, INPUT_PULLUP);
  pinMode(right, INPUT_PULLUP);
  pinMode(A0, OUTPUT);
  
  setTime(0, 0, 0, 0, 0 ,2000); // set dummy time so it can be used for blinking numbers
  
  lcd.begin(16, 2);  
  lcd.setCursor(0,0);
  lcd.print(" <Time>  <Alarm> ");
  sprintf(aika, "%02d:%02d:%02d  --:--", hms[0], hms[1], hms[2]);
  lcd.setCursor(0, 1);
  lcd.print(aika);

  // set time 
  for (int i = 0; i <= 2; i++) { 
    while (digitalRead(nxt) == HIGH) {
      if (second() % 2 == 0) { // if seconds even number blink number
        if (i == 0) {
          sprintf(aika, "  :%02d:%02d  --:--", hms[1], hms[2]);
        }
        else if (i == 1) {
          sprintf(aika, "%02d:  :%02d  --:--", hms[0], hms[2]);
        }
        else if (i == 2) {
          sprintf(aika, "%02d:%02d:    --:--", hms[0], hms[1]);
        }
      }
      else { // if seconds odd number show number
        sprintf(aika, "%02d:%02d:%02d  --:--", hms[0], hms[1], hms[2]);
      }
      lcd.setCursor(0, 1);
      lcd.print(aika);
      if (digitalRead(set) == LOW) {
        if (i == 0) {
          if (hms[i] < 23){hms[i] += 1;}
          else {hms[i] = 0;}
        }
        else {
          if (hms[i] < 59){hms[i] += 1;}
          else {hms[i] = 0;}
        }
        while (digitalRead(set) == LOW) {}
      }
    }
    while (digitalRead(nxt) == LOW) {}
  }
        
  setTime(hms[0], hms[1], hms[2], 0, 0 ,2000);
  hms[0] = 25, hms[2] = 0;
}

void loop() {
  
  if (hms[0] == 25) {
    sprintf(aika, "%02d:%02d:%02d  --:--", hour(), minute(), second());
  }
  else {
    sprintf(aika, "%02d:%02d:%02d  %02d:%02d", hour(), minute(), second(), hms[0], hms[1]);
  }
  lcd.setCursor(0, 1);
  lcd.print(aika);


  if ((hms[0] == hour()) and (hms[1] == minute()) and (hms[2] == second())) {
    haly = 1;
  }
  else if (haly != 0 ) {
    tone(buzzer, ((200 * (second() % 2)) + (300 * (1 + tch)))); 
    if (digitalRead(mid) == LOW) {
      tch = 1;
      servo.write(100);
    }
    else if (digitalRead(left) == LOW) {
      tch = 0;
      servo.write(50);
    }
    else if ((digitalRead(right) == LOW) and (tch == 0)) {
      noTone(buzzer);
      haly = 0;
      servo.write(0);
    }
  }  
  else if (digitalRead(nxt) == LOW) {
    if (hms[0] == 25) {
      hms[0] = 0, hms[1] = 0;
    }
    else {
      hms[0] = 25;
    }
    while (digitalRead(nxt) == LOW); {}
   
    
    if (hms[0] != 25) {
      for (int i = 0; i <= 1; i++) { // go throught ints in hms to set time for alarm
        while (digitalRead(nxt) == HIGH) {
          
          if (second() % 2 == 0) { // if seconds even number blink number
            if (i == 0) {
              sprintf(aika, "%02d:%02d:%02d    :%02d", hour(), minute(), second(), hms[1]);
            }
            else { // if seconds odd number show number
              sprintf(aika, "%02d:%02d:%02d  %02d:  ", hour(), minute(), second(), hms[0]);
            }
          }
          else {
            sprintf(aika, "%02d:%02d:%02d  %02d:%02d", hour(), minute(), second(), hms[0], hms[1]);
          }      
          lcd.setCursor(0, 1);
          lcd.print(aika);
          if (digitalRead(set) == LOW) {
            if (i == 0) {
              if (hms[i] < 23) {hms[i] += 1;}
              else {hms[i] = 0;}
            }
            else {
              if (hms[i] < 59) {hms[i] += 1;}
              else {hms[i] = 0;}
            }
            while (digitalRead(set) == LOW) {}
          }
        }
        while (digitalRead(nxt) == LOW) {}
      }
    }
  }
}